import './App.css';

import React from "react";
import {Route,Switch} from "react-router-dom";
import LandingPage from "./views/LandingPage/LandingPage";

function App() {
  return (
      <>
          <Switch>
              <Route path="/signUp" exact>

              </Route>
              <Route path="/login" exact>

              </Route>
              <Route path="/" exact>
                  <LandingPage/>
              </Route>
          </Switch>
      </>
  );
}

export default App;
