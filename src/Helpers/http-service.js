import {API_URL} from "../env";
import axios from "axios";

if (sessionStorage.getItem('token')) {
    axios.defaults.headers.common["Authorization"] =
        "Bearer " + JSON.parse(sessionStorage.getItem('token'));
}

class httpService {
    static setToken = (token) => {
        axios.defaults.headers.common["Authorization"] = "Bearer " + token;
    };
    configResponse=(res)=>{
    if(res.status === 200)
        {
            return res.data;
        }
        else
        {
            return res.data.description?res.data:{success:false,description:"Internal Server Error"};
        }
    };
    get = async (url, params) => this.configResponse(await axios.get(`${API_URL}${url}`, {params}));

    post = async (url, data) => this.configResponse(await axios.post(`${API_URL}${url}`, data));

    put = async (url, data, params) => this.configResponse(await axios.put(`${API_URL}${url}`, data, {params}));

    delete = async (url, params) => this.configResponse(await axios.delete(`${API_URL}${url}`, {params}));

    gets = async (url, params) => this.configResponse(await axios.get(`${API_URL}${url}`, {params}));

   }
export default new httpService();