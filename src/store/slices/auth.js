import {createSlice} from "@reduxjs/toolkit";

const authSlice = createSlice({
    name:'auth',
    initialState:{ isAuthenticated: !!JSON.parse(sessionStorage.getItem('token')),name:null},
    reducers: {
        login(state,actions) {
            if(actions.payload.token)
            {
                state.isAuthenticated = true;
                state.name=actions.payload.name;
            }
        },
        logout(state) {
            state.isAuthenticated = false;
            state.token=null;
            sessionStorage.clear();
        },
        signUp(state)
        {

        }
    }
});

export const authActions = authSlice.actions;
export default authSlice.reducer;